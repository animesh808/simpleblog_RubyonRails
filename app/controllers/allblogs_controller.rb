class AllblogsController < ApplicationController

    def index
        @allblogs = Allblog.all
    end
    
    def show
        @allblog = Allblog.find(params[:id])
        @comment = Comment.new
        @comment.blog_id = @allblog.id
        @comments = Comment.where("blog_id = '#{@allblog.id}'")
    end
    
    def new
        @allblog = Allblog.new
    end

    def edit
        @allblog = Allblog.find(params[:id])
    end

    def update
        @allblog = Allblog.find(params[:id])
        if @allblog.update_attributes(allblog_param)
            redirect_to :action => 'show', :id => @allblog
        else
            @allblogs = Allblog.all
            render :action => 'edit'
        end
    end
    
    def create
        @allblog  = Allblog.new(allblog_param)
        
        if @allblog.save
            redirect_to :action => 'index'
        else
            @allblogs = Allblog.all
            render :action => 'new'
        end
    end

    def destroy
        @allblog = Allblog.find(params[:id]).destroy
        redirect_to :action => 'index'
    end

    def allblog_params
        params.require(:allblogs).permit(:user_id, :headline, :text, :description, :imagepath)
    end


    def allblog_param
        params.require(:allblog).permit(:user_id, :headline, :text, :description, :imagepath)
    end
end
