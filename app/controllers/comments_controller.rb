class CommentsController < ApplicationController

    def create
        @comment  = Comment.new(cmnt_param)

        if @comment.save!
            redirect_to :back
        else
            @comments = Comment.all
            render :action => 'allblogs/show'
        end
    end

    def delete
        @comment = Comment.find(params[:id]).destroy
        redirect_to :action => 'allblogs/show'
    end

    def cmnt_param
        params.require(:comment).permit(:blog_id, :comment)
    end

end
