Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  
  resources :allblogs do
    resources :comments
  end

end
