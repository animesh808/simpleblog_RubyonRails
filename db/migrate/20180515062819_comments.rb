class Comments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :blog_id
      t.text :comment
    end
  end
end
