class Users < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.column :email, :string, :null => false
      t.column :password, :string, :null => false
      t.timestamps null: false
    end
  end
end
