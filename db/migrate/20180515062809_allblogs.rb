class Allblogs < ActiveRecord::Migration
  def change
    create_table :allblogs do |t|
      t.string :user_id
      t.string :headline
      t.string :text
      t.text :description
      t.string :imagepath
      t.timestamps null: false
    end
  end
end
